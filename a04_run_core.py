# ==============================================================
'''
SCRIPT:
	a04_run_core.py
	
PURPOSE:
    - Copy SQL data from S3 to gcp
    - Rename files to remove '000' before '.parquet'
    - Run Sean's core code to get avg spd and combine parquet files
    
    - Resources
        - Mass rename files in gcp:
        https://stackoverflow.com/questions/27166441/mass-rename-objects-on-google-cloud-storage
    
INPUTS:
    
OUTPUTS:
	
	
DEVELOPMENT:
    Version 1.0 - Joseph Nguyen | 15Apr2020
    ---
'''
# ==============================================================
# -------------------------------------------------


import os, sys, importlib
import pandas as pd
import numpy as np
import subprocess
from time import time
from datetime import date, datetime, timedelta
from google.cloud import bigquery

sys.path.insert(0, '/home/jovyan/a01_repos/wx_tools')
import wx_utils as utl
importlib.reload(sys.modules['wx_utils'])

# Intialise argo
# subprocess.call('~/init.sh', shell=True)
os.environ['GOOGLE_APPLICATION_CREDENTIALS']=f"/home/jovyan/.config/gcloud/legacy_credentials/{os.getenv('JUPYTERHUB_USER')}/adc.json"


# ==============================================================
# Paths
# ==============================================================
# # For Rohit
# # S3
# path_s3_data = 's3://data-preprod-redshift-exports/Joe/decision-engine'
# path_s3_resp = f'{path_s3_data}/response'
# path_s3_prev = f'{path_s3_data}/previous'

# path_s3_resp_files = f'{path_s3_resp}/*.parquet'
# path_s3_prev_files = f'{path_s3_prev}/*.parquet'

# # Output
# path_s3_cmd_base_all = f'{path_s3_data}/combine_cmd_base_all_20200508.csv'

# # GCP
# path_gcp_data = 'gs://wx-decision-engine/data/model_data'
# path_gcp_resp = f'{path_gcp_data}/response_back_20200508'
# path_gcp_prev = f'{path_gcp_data}/previous_back_20200508'

# path_gcp_resp_files = f'{path_gcp_resp}/*.parquet'
# path_gcp_prev_files = f'{path_gcp_prev}/*.parquet'

# path_gcp_otb_resp = f'{path_gcp_data}/response_back_otb_20200508'
# path_gcp_otb_prev = f'{path_gcp_data}/previous_back_otb_20200508'
# path_gcp_otb_comb = f'{path_gcp_data}/combine_back_otb_20200508'
# path_gcp_cmd_base = f'{path_gcp_data}/cmd_base/2020-05-08'

# S3
path_s3_data = 's3://data-preprod-redshift-exports/Joe/decision-engine'
path_s3_resp = f'{path_s3_data}/response'
path_s3_prev = f'{path_s3_data}/previous'

path_s3_resp_files = f'{path_s3_resp}/*.parquet'
path_s3_prev_files = f'{path_s3_prev}/*.parquet'

# GCP
path_gcp_data = 'gs://wx-decision-engine/data/model_data'
path_gcp_resp = f'{path_gcp_data}/response_back_20200415'
path_gcp_prev = f'{path_gcp_data}/previous_back_20200415'

path_gcp_resp_files = f'{path_gcp_resp}/*.parquet'
path_gcp_prev_files = f'{path_gcp_prev}/*.parquet'

path_gcp_otb_resp = f'{path_gcp_data}/response_back_otb_20200416'
path_gcp_otb_prev = f'{path_gcp_data}/previous_back_otb_20200416'
path_gcp_otb_comb = f'{path_gcp_data}/combine_back_otb_20200416'
path_gcp_cmd_base = f'{path_gcp_data}/cmd_base/2020-04-16'

# Local
path_local_data = '/home/jovyan/a02_projects/a05_bws_model_training/data'


# ==============================================================
# Copy S3 to GCP
# ==============================================================
# utl.copy_gsutil(path_s3_resp_files, path_gcp_resp)
# utl.copy_gsutil(path_s3_prev_files, path_gcp_prev)

# # Rename files: remove '000' before '.parquet'
# rename_in = '000.parquet'
# rename_out = '.parquet'
# utl.gcp_rename_files(path_gcp_resp_files, rename_in, rename_out)
# utl.gcp_rename_files(path_gcp_prev_files, rename_in, rename_out)


# -------------------------------------------------
# Copy gcp
# -------------------------------------------------
# utl.copy_gsutil('gs://wx-decision-engine/data/ss_band_new.csv', './data/ss_band.csv')
# utl.copy_gsutil('gs://wx-decision-engine/data/cvm_history/2019-06-06/2019_04_02_BWS_cvm.parquet', './data/cvm1.parquet')
# utl.copy_gsutil('gs://wx-decision-engine/data/cvm_history/2019-09-04/2019_07_02.parquet', './data/cvm2.parquet')

spendbandlist = [
    'gs://wx-decision-engine/data/spend_band_history/spend_band_2019-07-29.parquet',
    'gs://wx-decision-engine/data/spend_band_history/spend_band_2019-08-05.parquet',
    'gs://wx-decision-engine/data/spend_band_history/spend_band_2019-08-12.parquet',
    'gs://wx-decision-engine/data/spend_band_history/spend_band_2019-08-19.parquet',
    'gs://wx-decision-engine/data/spend_band_history/spend_band_2019-08-26.parquet',
    'gs://wx-decision-engine/data/spend_band_history/spend_band_2019-09-02.parquet',
    'gs://wx-decision-engine/data/spend_band_history/spend_band_2019-09-09.parquet',
    'gs://wx-decision-engine/data/spend_band_history/spend_band_2019-09-16.parquet',
    'gs://wx-decision-engine/data/spend_band_history/spend_band_2019-09-23.parquet',
    'gs://wx-decision-engine/data/spend_band_history/spend_band_2019-09-29.parquet',
    'gs://wx-decision-engine/data/spend_band_history/spend_band_2019-10-07.parquet',
    'gs://wx-decision-engine/data/spend_band_history/spend_band_2019-10-13.parquet',
    'gs://wx-decision-engine/data/spend_band_history/spend_band_2019-10-20.parquet',
    'gs://wx-decision-engine/data/spend_band_history/spend_band_2019-10-28.parquet',
    'gs://wx-decision-engine/data/spend_band_history/spend_band_2019-11-04.parquet',
    'gs://wx-decision-engine/data/spend_band_history/spend_band_2019-11-11.parquet',
    'gs://wx-decision-engine/data/spend_band_history/spend_band_2019-11-21.parquet',
    'gs://wx-decision-engine/data/spend_band_history/spend_band_2019-11-28.parquet',
    'gs://wx-decision-engine/data/spend_band_history/spend_band_2019-12-05.parquet',
#     'gs://wx-decision-engine/data/spend_band_history/spend_band_2019-12-12.parquet',
#     'gs://wx-decision-engine/data/spend_band_history/spend_band_2019-12-19.parquet',
#     'gs://wx-decision-engine/data/spend_band_history/spend_band_2019-12-26.parquet',
#     'gs://wx-decision-engine/data/spend_band_history/spend_band_2020-01-02.parquet',
#     'gs://wx-decision-engine/data/spend_band_history/spend_band_2020-01-09.parquet',
    'gs://wx-decision-engine/data/spend_band_history/spend_band_2020-01-16.parquet',
#     'gs://wx-decision-engine/data/spend_band_history/spend_band_2020-01-23.parquet',
#     'gs://wx-decision-engine/data/spend_band_history/spend_band_2020-01-30.parquet',
    'gs://wx-decision-engine/data/spend_band_history/spend_band_2020-02-06.parquet',
]

audiencelist=[
    'gs://wx-decision-engine/data/date/2019-08-01/incoming/lever-set/set1/data.parquet',
    'gs://wx-decision-engine/data/date/2019-08-08/incoming/lever-set/set1/data.parquet',
    'gs://wx-decision-engine/data/date/2019-08-15/incoming/lever-set/set3/data.parquet',
    'gs://wx-decision-engine/data/date/2019-08-22/incoming/lever-set/set3/data.parquet',
    'gs://wx-decision-engine/data/date/2019-08-29/incoming/lever-set/set3/data.parquet',
    'gs://wx-decision-engine/data/date/2019-09-05/incoming/lever-set/set3/data.parquet',
    'gs://wx-decision-engine/data/date/2019-09-12/incoming/lever-set/set3/data.parquet',
    'gs://wx-decision-engine/data/date/2019-09-19/incoming/lever-set/set3/data.parquet',
    'gs://wx-decision-engine/data/date/2019-09-26/incoming/lever-set/set3/data.parquet',
    'gs://wx-decision-engine/data/date/2019-10-03/incoming/lever-set/set3/data.parquet',
    'gs://wx-decision-engine/data/date/2019-10-10/incoming/lever-set/set3/data.parquet',
    'gs://wx-decision-engine/data/date/2019-10-17/incoming/lever-set/set3/data.parquet',
    'gs://wx-decision-engine/data/date/2019-10-24/incoming/lever-set/set3/data.parquet',
    'gs://wx-decision-engine/data/date/2019-10-31/incoming/lever-set/set3/data.parquet',
    'gs://wx-decision-engine/data/date/2019-11-07/incoming/lever-set/set1/data.parquet',
    'gs://wx-decision-engine/data/date/2019-11-14/incoming/lever-set/set1/data.parquet',
    'gs://wx-decision-engine/data/date/2019-11-21/incoming/lever-set/set1/data.parquet',
    'gs://wx-decision-engine/data/date/2019-11-28/incoming/lever-set/set1/data.parquet',
    'gs://wx-decision-engine/data/date/2019-12-05/incoming/lever-set/set1/data.parquet',
#     'gs://wx-decision-engine/data/date/2019-12-12/incoming/lever-set/set1/data.parquet',
#     'gs://wx-decision-engine/data/date/2019-12-19/incoming/lever-set/set1/data.parquet',
#     'gs://wx-decision-engine/data/date/2019-12-26/incoming/lever-set/set3/data.parquet',
#     'gs://wx-decision-engine/data/date/2020-01-02/incoming/lever-set/set3/data.parquet',
#     'gs://wx-decision-engine/data/date/2020-01-09/incoming/lever-set/set3/data.parquet',
    'gs://wx-decision-engine/data/date/2020-01-16/incoming/lever-set/set3/data.parquet',
#     'gs://wx-decision-engine/data/date/2020-01-23/incoming/lever-set/set3/data.parquet',
#     'gs://wx-decision-engine/data/date/2020-01-30/incoming/lever-set/set3/data.parquet',
    'gs://wx-decision-engine/data/date/2020-02-06/incoming/lever-set/set3/data.parquet'
]

# -------------------------------------------------
# For rohit
# -------------------------------------------------
# spendbandlist = [
#     'gs://wx-decision-engine/data/spend_band_history/spend_band_2020-01-16.parquet',
#     'gs://wx-decision-engine/data/spend_band_history/spend_band_2020-02-06.parquet',
#     'gs://wx-decision-engine/data/spend_band_history/spend_band_2020-02-20.parquet',
#     'gs://wx-decision-engine/data/spend_band_history/spend_band_2020-02-27.parquet',
#     'gs://wx-decision-engine/data/spend_band_history/spend_band_2020-03-05.parquet',
#     'gs://wx-decision-engine/data/spend_band_history/spend_band_2020-03-12.parquet',
# ]
# date_ls = [
#     "2020-01-16",
#     "2020-02-06",
#     "2020-02-20",
#     "2020-02-27",
#     "2020-03-05",
#     "2020-03-12",
# ]
# query = '''select crn from wx_decision_engine.incoming where date = "'''
# audiencelist = [query + d + '"' for d in date_ls]
# audiencelist

# i = 0
# t_start = utl.Timer()
# df = pd.read_gbq(f'{audiencelist[i]}', project_id="wx-bq-poc")
# utl.Timer(t_start)
# t_start = utl.Timer()
# df = pd.read_gbq(f'{audiencelist[i]}', project_id="wx-bq-poc", use_bqstorage_api=True)
# utl.Timer(t_start)
# #^ 16 sec -> 3 sec

# ==============================================================
# Functions (from Sean)
# ==============================================================
def add_date(date_str, days_delta):
    dt_obj = datetime.strptime(date_str, '%Y-%m-%d')
    delta = timedelta(days=days_delta)
    return (dt_obj + delta).strftime("%Y-%m-%d")


def download_files(ref_dt, path_gcp_resp, path_gcp_prev):
    utl.copy_gsutil(f'{path_gcp_resp}/{ref_dt}.parquet', './data/response.parquet')
    utl.copy_gsutil(f'{path_gcp_prev}/{ref_dt}.parquet', './data/previous.parquet')
    return None


def combine_files():
    merge_fea = ['offer_id', 'campaign_duration']
    drop_fea = ['ref_dt', 'selection', 'action', 'offer_start_dt',
                'offer_end_dt', 'var_2', 'sample_weight']

    previous = pd.read_parquet("./data/previous.parquet")
    response = pd.read_parquet("./data/response.parquet")
    
    # Filter to random groups
    # *** UNCOMMENT FOR MODELLING ***
#     response = response[response['var_2'].isin(['RO', 'RN'])]
    
    previous['sample_weight'] = previous['sample_weight'].values.astype('float')
    response['sample_weight'] = response['sample_weight'].values.astype('float')
    
    dur = (pd.to_datetime(previous.offer_end_dt) -
           pd.to_datetime(previous.offer_start_dt))
    
    previous['campaign_duration'] = dur.dt.days + 1
    previous['offer_start_dt'] = pd.to_datetime(previous.offer_start_dt,
                                                format='%Y-%m-%d %H:%M:%S')
    
    offer_date = previous['offer_start_dt'].unique()
    res_list = []
    for d in offer_date:
        date = pd.to_datetime(str(d)) .strftime('%Y-%m-%d')
        
        ## join_offer_helper ## 
        temp = join_offer_helper(previous[previous.offer_start_dt == d], d, merge_fea, prev=True)
        res_list.append(temp)
    prev_offer = pd.concat(res_list, axis=0)
    
    dur = (pd.to_datetime(response.offer_end_dt) - pd.to_datetime(
        response.offer_start_dt))
    response['campaign_duration'] = dur.dt.days + 1
    response = join_offer_helper(response, ref_dt, merge_fea)

    combine = response.merge(prev_offer.drop(drop_fea, axis=1),
                             on='crn').reset_index(drop=True)
    
    prev_offer.to_parquet('./data/previous_tb.parquet')
    response.to_parquet('./data/response_tb.parquet')
    combine.to_parquet('./data/combine_tb.parquet')
    return combine


def join_offer_helper(df, dt, merge_fea, prev=False):
    os.system(f'gsutil cp gs://wx-decision-engine/data/date/{dt}/processed/offer_tbl.parquet .')
    
    offer_table = pd.read_parquet('offer_tbl.parquet')
    offer_table.rename({'offerId': 'offer_id'}, axis=1, inplace=True)
    offer_table.campaign_duration = offer_table.campaign_duration.astype(int)
    res = df.merge(offer_table, how='left', on=merge_fea)
    if prev:
        res = rename_to_prev(res, offer_table)
    return res


def rename_to_prev(df, offer_table):
    maper = {'offer_id': "prev_offer_id"}
    for f in offer_table.columns:
        maper[f] = f'prev_{f}'
    df.rename(maper, axis=1, inplace=True)
    return df


def join_cmd(combine, i):
    combine_cmd = combine.copy()

    ## get_avg ##
    avg_df = get_avg(i)
    combine_cmd = pd.merge(combine_cmd, avg_df,'left',on='crn')
    ss_col = ['category', 'offerId', 'reward_type', 'reward_dollar',
                'reward_pct','hurdle_cat', 'reward_cat',
                'stretch_level_rel', 'spend_band']
    ss_band = pd.read_csv('./data/ss_band.csv', usecols=ss_col)
    ss_band['reward_dollar'] = ss_band['reward_dollar'].astype(str).str.replace('$', '').astype(float)
    ss_band.rename(columns={"offerId": "offer_id"}, inplace=True)

    combine_cmd.loc[:,'sales'] = np.where(combine_cmd.campaign_duration == 4,
                                    combine_cmd.sale_7day,
                                    combine_cmd.sale_14day / 2)

    ss_df = combine_cmd[combine_cmd.campaign_type.str.lower() == 'ss']
    non_ss_df = combine_cmd[combine_cmd.campaign_type.str.lower() != 'ss']

    ss_band.offer_id = ss_band.offer_id.astype(str).str.lower()
    ss_df = ss_df.merge(ss_band, on=['offer_id'], how='left')
    Lapsed_offers = ss_band[ss_band['spend_band'] == 'Lapsed/Inactive'].offer_id

    sel_record = np.logical_or(ss_df.offer_id.isna(),
                                ss_df.offer_id.isin(Lapsed_offers))
    save = ss_df[~sel_record]
    other = ss_df[sel_record]

    save.loc[:,'avg_spd'] = save['avg_basket']
    save.loc[:,'avg_spd'] = np.where(save['category'] == 'wine', save['avg_wine'],
                                save['avg_spd'])
    save.loc[:,'avg_spd'] = np.where(save['category'] == 'beer', save['avg_beer'],
                                save['avg_spd'])
    save.loc[:,'avg_spd'] = np.where(save['category'] == 'spirits',
                                save['avg_spirits'],
                                save['avg_spd'])

    temp = save['spend_band'].str.replace('$', '').str.split('~',expand=True).astype(float)
    save2 = save[(save.avg_spd >= temp[0]) & (save.avg_spd <= temp[1])]
    combine_cmd = pd.concat([non_ss_df, save2, other], axis=0, sort=False).reset_index(
        drop=True)

    combine_cmd.loc[:,'de_ref_dt'] = combine_cmd['ref_dt'].values.astype('str').copy()
    combine_cmd.ref_dt = combine_cmd.de_ref_dt.apply(lambda x: add_date(x, -11))
    combine_cmd.to_parquet('./data/combine_all.parquet')
    return combine_cmd


def get_avg(i):
#     query = f'select crn from wx_decision_engine.incoming where date = "{thu_dt}"'
#     df = pd.read_gbq(query, project_id="wx-bq-poc", use_bqstorage_api=True)
    df = pd.read_parquet(f'{audiencelist[i]}',columns=['crn'])

    if i < 5:
        df=pd.merge(df,cvm1,'left',on='crn')
    else:
        df=pd.merge(df,cvm2,'left',on='crn')
    df.columns=['crn','cvm']
    df.loc[df['cvm'].isnull(), 'cvm'] = 'MISS'
    
#     spendband = f'gs://wx-decision-engine/data/spend_band_history/spend_band_{thu_dt}.parquet'
#     df_spend_band = pd.read_parquet(spendband).replace(np.nan, 0.0)
    df_spend_band = pd.read_parquet(f'{spendbandlist[i]}').replace(np.nan, 0.0)
    for j in ['avg_basket', 'avg_wine', 'avg_spirits', 'avg_beer']:
        df_spend_band[j] = df_spend_band[j].astype(np.float)
    df = pd.merge(df, df_spend_band, 'left', on='crn')
    df.loc[df['avg_basket'].isnull(), 'avg_basket'] = 0.0
    df.loc[df['avg_wine'].isnull(), 'avg_wine'] = 0.0
    df.loc[df['avg_spirits'].isnull(), 'avg_spirits'] = 0.0
    df.loc[df['avg_beer'].isnull(), 'avg_beer'] = 0.0

    try:
        df = df.drop(['ref_dt'], axis=1)
    except:
        pass
    return df[['crn', 'cvm', 'avg_basket', 'avg_wine', 'avg_spirits', 'avg_beer']]


# ==============================================================
# Main
# ==============================================================
cvm1 = pd.read_parquet('./data/cvm1.parquet')
cvm2 = pd.read_parquet('./data/cvm2.parquet')

subprocess.call(f'gsutil ls {path_gcp_resp} > ./data/src_list.txt', shell=True)
src_list = pd.read_table('./data/src_list.txt', header=None)[0].tolist()
i = 0

# txt = src_list[0]
# ref_dt = txt.rsplit('/')[-1].partition('.')[0]
# query = f'select crn from wx_decision_engine.incoming where date = "{ref_dt}"'
# query

df_ls = []
for txt in src_list:
    ref_dt = txt.rsplit('/')[-1].partition('.')[0]
    print(f'ref_dt: {ref_dt} | i: {i}')
    
    t1 = time()
    download_files(ref_dt, path_gcp_resp, path_gcp_prev)
    print(time() - t1)
    combine = combine_files()
    print(time() - t1)
    combine_cmd = join_cmd(combine, i)
    print(time() - t1)
    
    i += 1
    
#     # Unique ref_dt, offer_id, spend_band, spend_hurdle
#     cols = [
#         "ref_dt", 
#         "de_ref_dt", 
#         "offer_id",
#         "campaign_type",
#         "spend_band", 
#         "spend_hurdle", 
#         "reward", 
#         "multiplier"
#     ]
#     combine_unq = combine_cmd[cols].drop_duplicates()
    
#     ## All ref_dt combined df (list)
#     df_ls.append(combine_unq)
    
    ################### Upload files to gcp #################
    path_gcp_otb_resp_file = f'{path_gcp_otb_resp}/{ref_dt}.parquet'
    path_gcp_otb_prev_file = f'{path_gcp_otb_prev}/{ref_dt}.parquet'
    path_gcp_otb_comb_file = f'{path_gcp_otb_comb}/{ref_dt}.parquet'
    path_gcp_cmd_base_file = f'{path_gcp_cmd_base}/{ref_dt}.parquet'

    utl.copy_gsutil('./data/response_tb.parquet', path_gcp_otb_resp_file)
    utl.copy_gsutil('./data/previous_tb.parquet', path_gcp_otb_prev_file)
    utl.copy_gsutil('./data/combine_tb.parquet', path_gcp_otb_comb_file)
    utl.copy_gsutil('./data/combine_all.parquet', path_gcp_cmd_base_file)

# # All ref_dt combined df
# combine_cmd_all = pd.concat(df_ls)
# # combine_cmd_all.to_parquet('./data/combine_cmd_all.parquet')
# combine_cmd_all.to_csv('./data/combine_cmd_all.csv', index=False)

# # Copy to redshift (to then load into redshift)
# # utl.copy_gsutil('./data/combine_cmd_all.parquet', path_s3_cmd_base_all)
# utl.copy_gsutil('./data/combine_cmd_all.csv', path_s3_cmd_base_all)

combine_cmd_all.shape
combine.shape
combine_cmd_all.head()

combine_cmd.shape
combine_cmd.head()
combine_cmd.columns == 'sales'

# ==============================================================
'''
SCRIPT:
	a05_basket_ss_columns.py
	
PURPOSE:
    - Get columns for basket SS
    
INPUTS:
    
OUTPUTS:
	
	
DEVELOPMENT:
    Version 1.0 - Joseph Nguyen | 21May2020
    ---
'''
# ==============================================================
# -------------------------------------------------

import os, sys, importlib
import pandas as pd
import numpy as np
import subprocess
from time import time
from datetime import date, datetime, timedelta
from google.cloud import bigquery

sys.path.insert(0, '/home/jovyan/a01_repos/wx_tools')
import Wx_Utils as utl
importlib.reload(sys.modules['Wx_Utils'])

os.environ['GOOGLE_APPLICATION_CREDENTIALS']=f"/home/jovyan/.config/gcloud/legacy_credentials/{os.getenv('JUPYTERHUB_USER')}/adc.json"

# ==============================================================
# Paths
# ==============================================================
# S3
path_s3_data = 's3://data-preprod-redshift-exports/Joe/decision-engine'
path_s3_resp = f'{path_s3_data}/response'
path_s3_prev = f'{path_s3_data}/previous'

path_s3_resp_files = f'{path_s3_resp}/*.parquet'
path_s3_prev_files = f'{path_s3_prev}/*.parquet'

# Output
# path_s3_cmd_base_all = f'{path_s3_data}/combine_cmd_base_all_20200508.csv'

# GCP
path_gcp_data = 'gs://wx-decision-engine/data/model_data'
path_gcp_resp = f'{path_gcp_data}/response_back_20200508'
path_gcp_prev = f'{path_gcp_data}/previous_back_20200508'

path_gcp_resp_files = f'{path_gcp_resp}/*.parquet'
path_gcp_prev_files = f'{path_gcp_prev}/*.parquet'

# path_gcp_otb_resp = f'{path_gcp_data}/response_back_otb_20200508'
# path_gcp_otb_prev = f'{path_gcp_data}/previous_back_otb_20200508'
# path_gcp_otb_comb = f'{path_gcp_data}/combine_back_otb_20200508'
# path_gcp_cmd_base = f'{path_gcp_data}/cmd_base/2020-05-08'

# Local
path_local_data = '/home/jovyan/a02_projects/a05_isd/data'


# ==============================================================
# Copy S3 to GCP
# ==============================================================
utl.Copy_Gsutil(path_s3_resp_files, path_gcp_resp)
utl.Copy_Gsutil(path_s3_prev_files, path_gcp_prev)

# Rename files: remove '000' before '.parquet'
rename_in = '000.parquet'
rename_out = '.parquet'
utl.gcp_rename_files(path_gcp_resp_files, rename_in, rename_out)
utl.gcp_rename_files(path_gcp_prev_files, rename_in, rename_out)

# -------------------------------------------------
# Copy gcp
# -------------------------------------------------
# Get spend_band, stretch_level_rel
utl.Copy_Gsutil('gs://wx-decision-engine/data/ss_band_new.csv', './data/ss_band.csv')

# cvm
utl.Copy_Gsutil('gs://wx-decision-engine/data/cvm_history/2019-06-06/2019_04_02_BWS_cvm.parquet', './data/cvm1.parquet')
utl.Copy_Gsutil('gs://wx-decision-engine/data/cvm_history/2019-09-04/2019_07_02.parquet', './data/cvm2.parquet')


# ==============================================================
# Functions (from Sean)
# ==============================================================
def add_date(date_str, days_delta):
    dt_obj = datetime.strptime(date_str, '%Y-%m-%d')
    delta = timedelta(days=days_delta)
    return (dt_obj + delta).strftime("%Y-%m-%d")


def download_files(ref_dt, path_gcp_resp, path_gcp_prev):
    utl.Copy_Gsutil(f'{path_gcp_resp}/{ref_dt}.parquet', './data/response.parquet')
    utl.Copy_Gsutil(f'{path_gcp_prev}/{ref_dt}.parquet', './data/previous.parquet')
    return None


def combine_files():
    merge_fea = ['offer_id', 'campaign_duration']
    drop_fea = ['ref_dt', 'selection', 'action', 'offer_start_dt',
                'offer_end_dt', 'var_2', 'sample_weight']

    previous = pd.read_parquet("./data/previous.parquet")
    response = pd.read_parquet("./data/response.parquet")
    
    # Filter to random groups
    # *** UNCOMMENT FOR MODELLING ***
#     response = response[response['var_2'].isin(['RO', 'RN'])]
    
    previous['sample_weight'] = previous['sample_weight'].values.astype('float')
    response['sample_weight'] = response['sample_weight'].values.astype('float')
    
    dur = (pd.to_datetime(previous.offer_end_dt) -
           pd.to_datetime(previous.offer_start_dt))
    
    previous['campaign_duration'] = dur.dt.days + 1
    previous['offer_start_dt'] = pd.to_datetime(previous.offer_start_dt,
                                                format='%Y-%m-%d %H:%M:%S')
    
    offer_date = previous['offer_start_dt'].unique()
    res_list = []
    for d in offer_date:
        date = pd.to_datetime(str(d)) .strftime('%Y-%m-%d')
        
        ## join_offer_helper ## 
        temp = join_offer_helper(previous[previous.offer_start_dt == d], d, merge_fea, prev=True)
        res_list.append(temp)
    prev_offer = pd.concat(res_list, axis=0)
    
    dur = (pd.to_datetime(response.offer_end_dt) - pd.to_datetime(
        response.offer_start_dt))
    response['campaign_duration'] = dur.dt.days + 1
    response = join_offer_helper(response, ref_dt, merge_fea)

    combine = response.merge(prev_offer.drop(drop_fea, axis=1),
                             on='crn').reset_index(drop=True)
    
    prev_offer.to_parquet('./data/previous_tb.parquet')
    response.to_parquet('./data/response_tb.parquet')
    combine.to_parquet('./data/combine_tb.parquet')
    return combine


def join_offer_helper(df, dt, merge_fea, prev=False):
    os.system(f'gsutil cp gs://wx-decision-engine/data/date/{dt}/processed/offer_tbl.parquet .')
    
    offer_table = pd.read_parquet('offer_tbl.parquet')
    offer_table.rename({'offerId': 'offer_id'}, axis=1, inplace=True)
    offer_table.campaign_duration = offer_table.campaign_duration.astype(int)
    res = df.merge(offer_table, how='left', on=merge_fea)
    if prev:
        res = rename_to_prev(res, offer_table)
    return res


def rename_to_prev(df, offer_table):
    maper = {'offer_id': "prev_offer_id"}
    for f in offer_table.columns:
        maper[f] = f'prev_{f}'
    df.rename(maper, axis=1, inplace=True)
    return df


def join_cmd(combine, thu_dt):
    combine_cmd = combine.copy()

    ## get_avg ##
    avg_df = get_avg(thu_dt)
    combine_cmd = pd.merge(combine_cmd, avg_df,'left',on='crn')
    ss_col = ['category', 'offerId', 'reward_type', 'reward_dollar',
                'reward_pct','hurdle_cat', 'reward_cat',
                'stretch_level_rel', 'spend_band']
    ss_band = pd.read_csv('./data/ss_band.csv', usecols=ss_col)
    ss_band['reward_dollar'] = ss_band['reward_dollar'].astype(str).str.replace('$', '').astype(float)
    ss_band.rename(columns={"offerId": "offer_id"}, inplace=True)

    combine_cmd.loc[:,'sales'] = np.where(combine_cmd.campaign_duration == 4,
                                    combine_cmd.sale_7day,
                                    combine_cmd.sale_14day / 2)

    ss_df = combine_cmd[combine_cmd.campaign_type.str.lower() == 'ss']
    non_ss_df = combine_cmd[combine_cmd.campaign_type.str.lower() != 'ss']

    ss_band.offer_id = ss_band.offer_id.astype(str).str.lower()
    ss_df = ss_df.merge(ss_band, on=['offer_id'], how='left')
    Lapsed_offers = ss_band[ss_band['spend_band'] == 'Lapsed/Inactive'].offer_id

    sel_record = np.logical_or(ss_df.offer_id.isna(),
                                ss_df.offer_id.isin(Lapsed_offers))
    save = ss_df[~sel_record]
    other = ss_df[sel_record]

    save.loc[:,'avg_spd'] = save['avg_basket']
    save.loc[:,'avg_spd'] = np.where(save['category'] == 'wine', save['avg_wine'],
                                save['avg_spd'])
    save.loc[:,'avg_spd'] = np.where(save['category'] == 'beer', save['avg_beer'],
                                save['avg_spd'])
    save.loc[:,'avg_spd'] = np.where(save['category'] == 'spirits',
                                save['avg_spirits'],
                                save['avg_spd'])

    temp = save['spend_band'].str.replace('$', '').str.split('~',expand=True).astype(float)
    save2 = save[(save.avg_spd >= temp[0]) & (save.avg_spd <= temp[1])]
    combine_cmd = pd.concat([non_ss_df, save2, other], axis=0, sort=False).reset_index(
        drop=True)

    combine_cmd.loc[:,'de_ref_dt'] = combine_cmd['ref_dt'].values.astype('str').copy()
    combine_cmd.ref_dt = combine_cmd.de_ref_dt.apply(lambda x: add_date(x, -11))
    combine_cmd.to_parquet('./data/combine_all.parquet')
    return combine_cmd


def get_avg(thu_dt):
    query = f'select crn from wx_decision_engine.incoming where date = "{thu_dt}"'
    df = pd.read_gbq(query, project_id="wx-bq-poc", use_bqstorage_api=True)
#     df = pd.read_parquet(f'{audiencelist[i]}',columns=['crn'])
    
    df=pd.merge(df,cvm2,'left',on='crn')
#     if i < 5:
#         df=pd.merge(df,cvm1,'left',on='crn')
#     else:
#         df=pd.merge(df,cvm2,'left',on='crn')
    df.columns=['crn','cvm']
    df.loc[df['cvm'].isnull(), 'cvm'] = 'MISS'

    df_spend_band = pd.read_parquet(f'{spendband}').replace(np.nan, 0.0)
    for j in ['avg_basket', 'avg_wine', 'avg_spirits', 'avg_beer']:
        df_spend_band[j] = df_spend_band[j].astype(np.float)
    df = pd.merge(df, df_spend_band, 'left', on='crn')
    df.loc[df['avg_basket'].isnull(), 'avg_basket'] = 0.0
    df.loc[df['avg_wine'].isnull(), 'avg_wine'] = 0.0
    df.loc[df['avg_spirits'].isnull(), 'avg_spirits'] = 0.0
    df.loc[df['avg_beer'].isnull(), 'avg_beer'] = 0.0

    try:
        df = df.drop(['ref_dt'], axis=1)
    except:
        pass
    return df[['crn', 'cvm', 'avg_basket', 'avg_wine', 'avg_spirits', 'avg_beer']]



# ==============================================================
# Main
# ==============================================================
cvm1 = pd.read_parquet('./data/cvm1.parquet')
cvm2 = pd.read_parquet('./data/cvm2.parquet')

thu_dt = '2020-03-12'
spendband = f'gs://wx-decision-engine/data/spend_band_history/spend_band_{thu_dt}.parquet'

download_files(ref_dt, path_gcp_resp, path_gcp_prev)
combine = combine_files()
combine_cmd = join_cmd(combine, thu_dt)
combine_cmd.shape

# Required columns for SS models (rdm, spd) (base-spd)
cols = ['avg_basket',
        'avg_beer',
        'avg_spirits',
        'avg_wine',
        'campaign_category',
        'campaign_duration',
        'campaign_level',
        'campaign_type',
        'cvm',
        'multiplier',
        'prev_campaign_level',
        'prev_open_flag',
        'prev_redeem_flag',
        'prev_sale_14day',
        'prev_sale_7day',
        'redeem_flag',
        'reward',
        'reward_cat',
        'reward_dollar',
        'reward_pct',
        'spend_hurdle',
        'stretch_level_rel',
        # Additional
        'spend_band',
        'crn',
        'ref_dt',
        'run_dt',
       ]
len(cols)

df_ss = combine_cmd[cols]
df_ss.dtypes
df_ss.head()

# -------------------------------------------------
# Testing
# -------------------------------------------------
# Query column values; specify values for basket SS
cond = (df_ss['campaign_type'] == 'ss') & (df_ss['campaign_level'] == 'store')

df_ss.loc[cond, 'campaign_category'].value_counts() # na
df_ss.loc[cond, 'campaign_duration'].value_counts() # 11
df_ss.loc[cond, 'campaign_level'].value_counts() # store
df_ss.loc[cond, 'campaign_type'].value_counts() # ss
df_ss.loc[cond, 'cvm'].value_counts() #
df_ss.loc[cond, 'multiplier'].value_counts() # 0.0
df_ss.loc[cond, 'multiplier'].value_counts() # 0.0
df_ss.loc[cond, 'prev_campaign_level'].value_counts() # store, category
df_ss.loc[cond, 'prev_open_flag'].value_counts() # 0,1
df_ss.loc[cond, 'prev_redeem_flag'].value_counts() # 0,1 
df_ss.loc[cond, 'prev_sale_14day'].value_counts() # 
df_ss.loc[cond, 'prev_sale_7day'].value_counts() # 
df_ss.loc[cond, 'redeem_flag'].value_counts() # 0,1 
df_ss.loc[cond, 'reward'].value_counts() # [dollars]
df_ss.loc[cond, 'reward_cat'].value_counts() # H,M,L
df_ss.loc[cond, 'reward_dollar'].value_counts() # [dollars]
df_ss.loc[cond, 'reward_pct'].value_counts() # 0.05, 0.10, 0.15
df_ss.loc[cond, 'spend_hurdle'].value_counts() # 
df_ss.loc[cond, 'stretch_level_rel'].value_counts() # 

cond = df_ss.loc[cond, 'reward_dollar'] != df_ss.loc[cond, 'reward']
cond.sum() # 0: columns identical

cols = ['spend_hurdle', 'reward', 'reward_pct', 'stretch_level_rel', 'spend_band']
df_ss.loc[cond, cols].head()
df_ss['xx_reward_pct'] = df_ss['reward'] / df_ss['spend_hurdle']
cols += ['xx_reward_pct']
df_ss.loc[cond, cols].head()

a = df_ss.loc[cond, 'xx_reward_pct'] != df_ss.loc[cond, 'reward_pct']
a.sum() # 0: reward_pct = reward / spend_hurdle


# Stretch level relative
ss_col = ['spend_band', 'average_spend', 'hurdle', 'stretch_level_rel']
ss_band = pd.read_csv('./data/ss_band.csv', usecols=ss_col)
ss_band.head()
(205 - 135) / 135


# Without previous
cols1 = ['avg_basket',
         'avg_beer',
         'avg_spirits',
         'avg_wine',
         'campaign_category',
         'campaign_duration',
         'campaign_level',
         'campaign_type',
         'cvm',
         'hurdle_cat',
         'multiplier',
         'redeem_flag',
         'reward',
         'reward_dollar',
         'spend_hurdle',
         'stretch_level_rel',
         'crn',
         'ref_dt',
        ]

df_ss1 = combine_all[cols1]
df_ss1.dtypes

df_ss1.loc[cond, 'hurdle_cat'].value_counts() # L,M,H
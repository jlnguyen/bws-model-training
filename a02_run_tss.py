# ==============================================================
'''
SCRIPT:
	a02_run_tss.py
	
PURPOSE:
    - Decision-engine tiered spend stretch (tss)
    - Join data using CMD2 pivoter
    - Run models: mdlTypeLs = ['open', 'rdm', 'spd', 'unsub']
    
    Original model config path (using CMD1)
    - gs://wx-personal/tzhao/TSS
    
INPUTS:
    
OUTPUTS:
	
	
DEVELOPMENT:
    Version 1.0 - Joseph Nguyen | 08Apr2020
    ---
'''
# ==============================================================
# -------------------------------------------------

import os, sys, importlib
import pandas as pd
import numpy as np
import subprocess

sys.path.insert(0, '/home/jovyan/a01_repos/wx_tools')
import Wx_Utils as utl
importlib.reload(sys.modules['Wx_Utils'])

# Intialise argo
subprocess.call('~/init.sh', shell=True)


# ==============================================================
# Copy data prep notebook
# ==============================================================
if False:
    utl.Copy_Gsutil('gs://wx-personal/tzhao/TSS/data_prep_tss.ipynb', '.')
    #^ Data copied as:
    # !gsutil cp data.parquet gs://wx-personal/tzhao/decision-engine-phase2/TSS/

    # -------------------------------------------------
    # Query data from gcs
    # -------------------------------------------------
    in_path_gcs_base_file = 'gs://wx-personal/tzhao/decision-engine-phase2/TSS/data.parquet'
    in_path_local_base_file = '/home/jovyan/a02_projects/a05_isd/data/data_tss.parquet'
    utl.Copy_Gsutil(in_path_gcs_base_file, in_path_local_base_file)

    # Query data
    df = pd.read_parquet(in_path_local_base_file)
    print(df.shape) # (1953651, 43)
    df.head(2)
    df['ref_dt'].value_counts() # 2019-02-17    1953651

    # Copy to working storage
    utl.Copy_Gsutil(
        in_path_gcs_base_file
        ,'gs://wx-personal/joe/a02_decision-engine/tss/data/base_tss.parquet'
    )


# ==============================================================
# Paths
# ==============================================================
# Files
file_base = 'base_tss.parquet'
file_cmd_sources = 'cmd_sources.txt'

# GCS
path_gcs_prj = 'gs://wx-personal/joe/a02_decision-engine/tss'
path_gcs_config = f'{path_gcs_prj}/config'
path_gcs_data = f'{path_gcs_prj}/data'
path_gcs_data_cmd = f'{path_gcs_data}/w_cmd'

path_gcs_base_file = f'{path_gcs_data}/{file_base}'
path_gcs_cmd_sources_file = f'{path_gcs_config}/{file_cmd_sources}'

# Local
path_local_prj = '/home/jovyan/a02_projects/a05_isd'
path_local_data = f'{path_local_prj}/data'
path_local_base_file = f'{path_local_data}/{file_base}'
path_local_cmd_sources_file = f'{path_local_data}/{file_cmd_sources}'

#__model run argos
mdlTypeLs = ['open', 'rdm', 'spd', 'unsub']
path_local_run_open = f'{path_local_prj}/back_run_open.yaml'

# Sample base with cmd
file_base_smpl = 'base_tss_smpl.parquet'
path_local_base_smpl_file = f'{path_local_data}/{file_base_smpl}'
path_gcs_base_smpl_file = f'{path_gcs_data}/{file_base_smpl}'
path_gcs_data_cmd_smpl = f'{path_gcs_data}/w_cmd_smpl'


# -------------------------------------------------
# Load
# -------------------------------------------------
# utl.Copy_Gsutil(path_gcs_base_file, path_local_base_file)

base = pd.read_parquet(path_local_base_file)
print(base.shape) # (1953651, 43)
base.head(2)

baseSmpl = base.sample(utl.Get_N_Smpl(base))
baseSmpl.to_parquet(path_local_base_smpl_file, index=False)
utl.Copy_Gsutil(path_local_base_smpl_file, path_gcs_base_smpl_file)

# cmd_sources
utl.Copy_Gsutil(path_local_cmd_sources_file, path_gcs_cmd_sources_file)

# Sample for excel generator
baseSmpl = base.sample(utl.Get_N_Smpl(base, 200000))
baseSmpl.to_parquet(path_gcs_base_smpl_file, index=False)
utl.Copy_Gsutil(path_local_base_2019_smpl_file, path_gcs_base_2019_smpl_file)


# ==============================================================
# CMD2 pivoter
# ==============================================================
utl.Call_Table_Pivoter(
    in_path=path_gcs_base_file
    ,out_path=path_gcs_data_cmd
    ,file_incl_sources=path_gcs_cmd_sources_file
    ,out_file_num=1
)
# Sample base w cmd
utl.Call_Table_Pivoter(
    in_path=path_gcs_base_smpl_file
    ,out_path=path_gcs_data_cmd_smpl
    ,file_incl_sources=path_gcs_cmd_sources_file
    ,out_file_num=1
)


# ==============================================================
# Create excel config
# ==============================================================
#> in ~/a01_repos/gen-akl-feature-config

# ==============================================================
# Model Build
# ==============================================================
for mt in mdlTypeLs[:-1]:
    path_local_run = f'{path_local_prj}/tss_run_{mt}.yaml'
    subprocess.call(f'argo submit {path_local_run}', shell=True)

path_local_run = f'{path_local_prj}/tss_run_open.yaml'
path_local_run = f'{path_local_prj}/tss_run_unsub.yaml'
path_local_run = f'{path_local_prj}/tss_run_spd.yaml'
path_local_run = f'{path_local_prj}/tss_run_rdm.yaml'
subprocess.call(f'argo submit {path_local_run}', shell=True)

!argo list | grep joe
!argo get joe-t-mp-rdm-hk6pc
!kubectl logs joe-tss-spd-z52xz-2657047991 main #> 'errlog_akl_05_reg_2.txt'
!argo delete joe-t-mp-rdm-g679m


# ==============================================================
# Exploratory
# ==============================================================
base = pd.read_parquet(path_local_base_file)
print(base.shape) # (3111277, 37)
base.head(2)


# -------------------------------------------------
# Binary response proportions
# -------------------------------------------------
utl.Show_Count_Perc(base, 'open_flag')
utl.Show_Count_Perc(base, 'redeem_flag')
utl.Show_Count_Perc(base, 'unsub_flag')

# Spend (regression)
base['tot_amt_incld_gst_promo'].describe().round(2)
base.groupby('redeem_flag')['tot_amt_incld_gst_promo'].describe()


# -------------------------------------------------
# Unsubcribe model
# -------------------------------------------------
utl.Copy_Gsutil('gs://wx-personal/joe/a02_decision-engine/isd/data/w_cmd/part-00000-f293d9b4-6619-4bd1-8260-76d4b4759aad-c000.snappy.parquet', './data/w_cmd_part.parquet')

baseCmd = pd.read_parquet('./data/w_cmd_part.parquet')
print(baseCmd.shape)

colsUnsub = [
    'f22_unique_cvm_unsub_cnt_bigw'
    ,'f22_unique_cvm_unsub_cnt_bws'
    ,'f22_unique_cvm_unsub_cnt_caltex'
    ,'f22_unique_cvm_unsub_cnt_fuelco'
    ,'f22_unique_cvm_unsub_cnt_supers'
    
    # numeric
    ,'f22_last_open_send_hr_diff_b4_unsub'
]
for c in colsUnsub:
    print(baseCmd[c].value_counts())


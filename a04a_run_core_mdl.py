# ==============================================================
'''
SCRIPT:
	a04a_run_core_mdl.py
	
PURPOSE:
    - CMD2 pivoter + akl model generation
    
INPUTS:
    
OUTPUTS:
	
	
DEVELOPMENT:
    Version 1.0 - Joseph Nguyen | 15Apr2020
    ---
'''
# ==============================================================
# -------------------------------------------------

import os, sys, importlib
import pandas as pd
import numpy as np
import subprocess

sys.path.insert(0, '/home/jovyan/a01_repos/wx_tools')
import Wx_Utils as utl
importlib.reload(sys.modules['Wx_Utils'])

# Intialise argo
subprocess.call('~/init.sh', shell=True)


# ==============================================================
# Paths
# ==============================================================
file_cmd_sources = 'cmd_sources.txt'

# GCP
path_gcp_data = 'gs://wx-decision-engine/data/model_data'
path_gcs_config = f'{path_gcp_data}/config'
path_gcp_cmd_base = f'{path_gcp_data}/cmd_base/2020-04-16'
path_gcs_data_cmd = f'{path_gcp_data}/w_cmd/2020-04-16'

path_gcp_cmd_base_files = f'{path_gcp_cmd_base}/*.parquet'
path_gcs_cmd_sources_file = f'{path_gcs_config}/{file_cmd_sources}'

# Local
path_local_prj = '/home/jovyan/a02_projects/a05_isd'
path_local_data = f'{path_local_prj}/data'
path_local_cmd_sources_file = f'{path_local_data}/{file_cmd_sources}'


# -------------------------------------------------
# Copy local to gcp
# -------------------------------------------------
utl.Copy_Gsutil(path_local_cmd_sources_file, path_gcs_cmd_sources_file)


# ==============================================================
# CMD2 pivoter
# ==============================================================
utl.Call_Table_Pivoter(
    in_path=path_gcp_cmd_base_files
    ,out_path=path_gcs_data_cmd
    ,file_incl_sources=path_gcs_cmd_sources_file
    ,out_file_num=50
)


# ==============================================================
# Model Build
# ==============================================================
mdl_type = [
    'open_1'
    ,'open_prev'
    ,'rdm_1'
    ,'rdm_prev'
    ,'rn_1'
    ,'rn_prev'
    ,'ro_1'
    ,'ro_prev'
    ,'unsub_1'
    ,'unsub_prev'
]
for mt in mdl_type:
    path_local_run = f'{path_local_prj}/core_run_{mt}.yaml'
    subprocess.call(f'argo submit {path_local_run}', shell=True)

mt = 'rn_prev'
path_local_run = f'{path_local_prj}/core_run_{mt}.yaml'
subprocess.call(f'argo submit {path_local_run}', shell=True)

!argo list | grep joe
!argo get joe-core-rn-kts28
# !kubectl logs joe-isd-unsub-2jw9r-2651246455 main > 'errlog_unsub_0414.txt'
!kubectl logs joe-core-open-prev-cnwdz-2578442163 main # preprocessing
!kubectl logs joe-core-rdm-klt7p-404240694 main # structure-creation

!argo get joe-core-open-hfwvv
!argo get joe-core-open-prev-cnwdz



# ==============================================================
# QA
# ==============================================================
test = pd.read_parquet('./data/combine_all.parquet')
test.shape
test.columns
test.head()
test.describe(include='all')
test['campaign_type'].value_counts()
test['prev_campaign_type'].value_counts()

test['sample_weight'].head()
test.dtypes


test['var_2'].value_counts()
# TO     504835
# TN      99357
# RO      72986
# TOC     56056
# TNC      9115
# RN       7861

# Filtered to var2 in ('rn', 'ro')
n_filtered = test[test['var_2'].isin(['RN', 'RO'])].shape[0]

print(f'Total data size (approx): {test.shape[0]*20}')
print(f'(rn, ro) data size (approx): {n_filtered * 20}')

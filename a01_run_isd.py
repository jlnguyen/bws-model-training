# ==============================================================
'''
SCRIPT:
	a01_run_isd.py   
	
PURPOSE:
    - Decision-engine instant discount (ISD)
    - Join data using CMD2 pivoter
    - Run models: mdlTypeLs = ['open', 'rdm', 'spd', 'unsub']
    
    Original model config path (using CMD1)
    - gs://wx-personal/tzhao/ID

INPUTS:
    
OUTPUTS:
	
	
DEVELOPMENT:
    Version 1.0 - Joseph Nguyen | 07Apr2020
    ---
'''
# ==============================================================
# -------------------------------------------------

import os, sys, importlib
import pandas as pd
import numpy as np
import subprocess
import matplotlib.pyplot as plt
import seaborn as sns

sys.path.insert(0, '/home/jovyan/a01_repos/wx_tools')
import wx_utils as utl
importlib.reload(sys.modules['wx_utils'])

# Intialise argo
subprocess.call('~/init.sh', shell=True)


# ==============================================================
# Paths
# ==============================================================
# -------------------------------------------------
# Inputs
# -------------------------------------------------
in_file_gcs_base = 'data.parquet'
in_path_gcs_prj = 'gs://wx-personal/tzhao/decision-engine-phase2/ID2'
in_path_gcs_base_file = f'{in_path_gcs_prj}/{in_file_gcs_base}'


# -------------------------------------------------
# Working storage
# -------------------------------------------------
# Files
file_base = 'base.parquet'
file_base_2019 = 'base_2019.parquet'
file_cmd_sources = 'cmd_sources.txt'

# GCS
path_gcs_prj = 'gs://wx-personal/joe/a02_decision-engine/isd'
path_gcs_config = f'{path_gcs_prj}/config'
path_gcs_data = f'{path_gcs_prj}/data'
path_gcs_data_cmd = f'{path_gcs_data}/w_cmd'

path_gcs_base_file = f'{path_gcs_data}/{file_base}'
path_gcs_cmd_sources_file = f'{path_gcs_config}/{file_cmd_sources}'

# Local
path_local_prj = '/home/jovyan/a02_projects/a05_isd'
path_local_data = f'{path_local_prj}/data'
path_local_base_file = f'{path_local_data}/{file_base}'
path_local_base_2019_file = f'{path_local_data}/{file_base_2019}'

path_local_base = f'{path_local_data}/data_id1.parquet'

#__model run argos
mdlTypeLs = ['open', 'rdm', 'spd', 'unsub']
path_local_run_open = f'{path_local_prj}/back_run_open.yaml'

# Sample base with cmd
file_base_2019_smpl = 'base_2019_smpl.parquet'
path_local_base_2019_smpl_file = f'{path_local_data}/{file_base_2019_smpl}'
path_gcs_base_2019_smpl_file = f'{path_gcs_data}/{file_base_2019_smpl}'
path_gcs_data_cmd_smpl = f'{path_gcs_data}/w_cmd_smpl'

# -------------------------------------------------
# Transfer
# -------------------------------------------------
# from input (tzhao)
utl.Copy_Gsutil(in_path_gcs_base_file, path_local_base_file)

# to working gcs
# utl.Copy_Gsutil(path_local_base_file, path_gcs_base_file)

# -------------------------------------------------
# Load
# -------------------------------------------------
base = pd.read_parquet(path_local_base_file)
print(base.shape) # (3111277, 37)
base.head(2)
baseColLs = base.columns.tolist()
print(baseColLs)
base['ref_dt'].describe()
base['ref_dt'].value_counts()
# 2018-07-22     660626
# 2019-05-19    1234551
# 2019-06-09    1216100

# Remove ref_dt before 2019-01-01 (start of cmd2 period)
base1 = base[base['ref_dt'] != '2018-07-22']
print(base1.shape)
base1.to_parquet(path_local_base_2019_file, index=False)
utl.Copy_Gsutil(path_local_base_2019_file, path_gcs_base_file)

# Sample for excel generator
baseSmpl = base1.sample(utl.Get_N_Smpl(base1, 200000))
baseSmpl.to_parquet(path_local_base_2019_smpl_file, index=False)
utl.Copy_Gsutil(path_local_base_2019_smpl_file, path_gcs_base_2019_smpl_file)


# ==============================================================
# CMD2 pivoter
# ==============================================================
utl.Call_Table_Pivoter(
    in_path=path_gcs_base_file
    ,out_path=path_gcs_data_cmd
    ,file_incl_sources=path_gcs_cmd_sources_file
    ,out_file_num=1
)
# Sample base w cmd
utl.Call_Table_Pivoter(
    in_path=path_gcs_base_2019_smpl_file
    ,out_path=path_gcs_data_cmd_smpl
    ,file_incl_sources=path_gcs_cmd_sources_file
    ,out_file_num=1
)


# ==============================================================
# Create excel config
# ==============================================================
#> in ~/a01_repos/gen-akl-feature-config

# ==============================================================
# Model Build
# ==============================================================
path_local_run = f'{path_local_prj}/isd_run_open.yaml'
path_local_run = f'{path_local_prj}/isd_run_unsub.yaml'
path_local_run = f'{path_local_prj}/isd_run_spd.yaml'
path_local_run = f'{path_local_prj}/isd_run_rdm.yaml'
subprocess.call(f'argo submit {path_local_run}', shell=True)

for mt in mdlTypeLs[1:]:
    path_local_run = f'{path_local_prj}/isd_run_{mt}.yaml'
    subprocess.call(f'argo submit {path_local_run}', shell=True)

!argo list | grep joe
!argo get joe-isd-spd-7wn2j
!kubectl logs joe-isd-unsub-2jw9r-2651246455 main > 'errlog_unsub_0414.txt'
!kubectl logs joe-isd-open-rwgjw-2704090267 main
!argo delete joe-isd-spd-f959c

utl.Copy_Gsutil('gs://wx-auto-ml/joe/id_unsub/2020-04-11/feature_selection/feature_selection_output_T.parquet', 'test.parquet')
test = pd.read_parquet('test.parquet')
test.shape
test.head(2)


# ==============================================================
# Exploratory
# ==============================================================
baseId1 = pd.read_parquet(path_local_base)
print(baseId1.shape) # (3111277, 37)
baseId1.head(2)
baseId1.columns

cols=['crn',
'fw_start',
'fy',
'wofy',
'ref_dt',
'campaign_code',
'lms_offer_id',
'segment_code',
'campaign_audience_type',
'tot_amt_incld_gst_promo',
'open_flag',
'activate_flag',
'unsub_flag',
'redeem_flag',
'multiplier',
'spend_hurdle',
'offer_id',
'reward',
'reward_val',
'reward_fin',
'tot_amt_incld_gst_l8w',
'reward_type',
'offer_desc',
'campaign_duration_wks',
'offer_duration_days',
'campaign_type',
'campaign_level',
'campaign_category',
'tot_l8w',
'tot_l4w',
'avg_basket',
'avg_wine',
'avg_spirits',
'avg_beer']
len(cols)

set(baseColLs) - set(cols)
set(cols) - set(baseColLs)

baseId1['campaign_type'].head()

# -------------------------------------------------
# Binary response proportions
# -------------------------------------------------
utl.Show_Count_Perc(baseId1, 'open_flag')
utl.Show_Count_Perc(baseId1, 'redeem_flag')
utl.Show_Count_Perc(baseId1, 'unsub_flag')

# Spend (regression)
baseId1['tot_amt_incld_gst_promo'].describe().round(2)
baseId1.groupby('redeem_flag')['tot_amt_incld_gst_promo'].describe()


# -------------------------------------------------
# Redemption Model
# -------------------------------------------------
baseId1[['reward', 'reward_pct']].describe().round(2)
baseId1['reward_pct'].head()
baseId1['reward_pct'].value_counts()

baseId1.groupby('redeem_flag')['reward_pct'].describe().round(2)
baseId1.groupby('redeem_flag')['reward'].describe().round(2)

sns.scatterplot(data=baseId1.sample(20000), x='reward', y='reward_pct')
sns.scatterplot(data=baseId1.sample(20000), x='redeem_flag', y='reward_pct')
sns.scatterplot(data=baseId1.sample(20000), x='redeem_flag', y='reward')

#^ {reward_pct} is 100% correlated with response {redeem_flag}

# -------------------------------------------------
# Unsubcribe model
# -------------------------------------------------
# utl.Copy_Gsutil('gs://wx-personal/joe/a02_decision-engine/isd/data/w_cmd_smpl/part-00000-468faef6-362e-4c8d-8375-f5c04651c071-c000.snappy.parquet', './data/isd_cmd_smpl.parquet')

baseCmd = pd.read_parquet('./data/isd_cmd_smpl.parquet')
print(baseCmd.shape)

colsUnsub = [
    'f22_unique_cvm_unsub_cnt_bigw'
    ,'f22_unique_cvm_unsub_cnt_bws'
    ,'f22_unique_cvm_unsub_cnt_caltex'
    ,'f22_unique_cvm_unsub_cnt_fuelco'
    ,'f22_unique_cvm_unsub_cnt_supers'
    
    ,'f22_unique_cat_unsub_cnt_bws'
    
    # numeric
    ,'f22_last_open_send_hr_diff_b4_unsub'
    ,'f22_unique_atl_unsub_cnt_bws'
]


for c in colsUnsub:
    print(baseCmd[c].value_counts())

baseCmd['f22_unique_atl_unsub_cnt_bws'].head()
baseCmd['f22_unique_cat_unsub_cnt_bws'].head()

baseCmd.astype(str).groupby(['unsub_flag', 'f22_unique_cat_unsub_cnt_bws']).size()
baseCmd['unsub_flag'].value_counts(dropna=False)
baseCmd['f22_unique_cat_unsub_cnt_bws'].value_counts(dropna=False)



# ==============================================================
# Exploratory: base data {29 May 2020}
# ==============================================================
# pd.reset_option('max_columns')
pd.set_option('display.max_columns', None)
# pd.reset_option('max_rows')
# pd.set_option('display.max_rows', None)

base = pd.read_parquet(path_local_base)
base['crn'] = base['crn'].values.astype(str)
base.shape
base.head(3)

base.columns
base.dtypes

base['cvm'].value_counts()
base['segment_code'].value_counts()
base['ref_dt'].value_counts()
base['fw_start'].value_counts()
base['campaign_code'].value_counts()

# base.drop(columns=['cvm'], inplace=True)
base = pd.merge(base, cvm, how='left', on='crn')
base['cvm'] = base['cvm'].fillna('MISS')

# Single ref_dt
cond = base['ref_dt'].isin(['2019-05-19', '2019-06-09'])
base1 = base[cond]
base1.shape
base1.head(3)
base1['fw_start'].value_counts()

# CVM
# !gsutil cp gs://wx-decision-engine/data/cvm_history/2019-09-04/2019_07_02.parquet data/bws_cvm.parquet
!gsutil cp gs://wx-decision-engine/data/cvm_history/2019-06-06/2019_04_02_BWS_cvm.parquet data/bws_cvm.parquet

# CVM
cvm = pd.read_parquet('data/bws_cvm.parquet')
cvm.columns = ['crn', 'cvm']

# base1.drop(columns=['cvm'], inplace=True)
base1 = pd.merge(base1, cvm, how='left', on='crn')
base1['cvm'] = base1['cvm'].fillna('MISS')
base1.


# cvm groups
base1['cvm'].value_counts()
base1['segment_code'].value_counts()
base1.tail(3)

# cvm groups for a campaign code
base1['campaign_code'].value_counts()
cond = (base1['campaign_code'] == 'BCV-2507')
base1.loc[cond, 'cvm'].value_counts()

cond = (base['campaign_code'] == 'BCV-1744')
cond = (base['campaign_code'] == 'BCV-2507')
base.loc[cond, 'lms_offer_id'].value_counts().sort_index()

# cond = (base['campaign_code'] == 'BCV-1744') & (base['lms_offer_id'].isin(219893))
cond = (base['campaign_code'] == 'BCV-2507') & (base['lms_offer_id'].isin([219893]))
base.loc[cond, 'ref_dt'].value_counts()
a = base.loc[cond, 'cvm'].value_counts() / base[cond].shape[0]
a.sort_index()


# Trained data
base1['tot_l4w'].isnull().sum()
base1['tot_l8w'].isnull().sum()

cond = (base1['tot_l4w'] == 0)
base1.loc[cond, 'spend_hurdle'].value_counts()
base1.loc[cond, 'spend_hurdle'].value_counts()

base1['tot_l4w_grp'] = np.where(
    base1['tot_l4w'] == 0, '0. 0',
    np.where(base1['tot_l4w'] < 10, '1. 10',
    np.where(base1['tot_l4w'] < 20, '2. 20',
    np.where(base1['tot_l4w'] < 50, '3. 50',
    np.where(base1['tot_l4w'] < 100, '4. 100',
             '5. > 100'
            ))))
)

cond = (base1['spend_hurdle'] == 40) & (base1['reward'] == 10)
base1[cond].groupby('tot_l4w_grp')[['redeem_flag', 'tot_amt_incld_gst_promo']].describe().round(2)


cond = (base1['spend_hurdle'] == 40)
base1.loc[cond, 'tot_l4w'].value_counts()
base1


base1['spend_hurdle'].value_counts()

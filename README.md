# README #

### What is this repository for? ###

* Training models for Decision Engine MMM (BWS)
* Using CMD2, AKL 0.5
* Models:
	* Core
	* Instant Discount
	* Tiered Spend Stretch
	* Tiered Multiplier

### How do I get set up? ###

* Run main for corresponding model types:
	* Instant Discount: a01_run_isd.py
	* Tiered Spend Stretch: a02_run_tss.py
	* Tiered Multiplier: a03_run_t_mp.py
	* Core: a04_run_core.py

* Use files in "core_sql" to generate data for core models

### Contribution guidelines ###


### Who do I talk to? ###

* Joe Nguyen: jnguyen7@woolworths.com.au

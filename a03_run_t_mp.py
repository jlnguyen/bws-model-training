# ==============================================================
'''
SCRIPT:
	a03_run_t_mp.py
	
PURPOSE:
    - Decision-engine tiered multiplier (tmp)
    - Join data using CMD2 pivoter
    - Run models: mdlTypeLs = ['open', 'rdm', 'spd', 'unsub']
    
    Original model config path (using CMD1)
    - gs://wx-personal/alexander/tripleM/
    
INPUTS:
    
OUTPUTS:
	
	
DEVELOPMENT:
    Version 1.0 - Joseph Nguyen | 08Apr2020
    ---
'''
# ==============================================================
# -------------------------------------------------

import os, sys, importlib
import pandas as pd
import numpy as np
import subprocess

sys.path.insert(0, '/home/jovyan/a01_repos/wx_tools')
import Wx_Utils as utl
# importlib.reload(sys.modules['Wx_Utils'])

# Intialise argo
subprocess.call('~/init.sh', shell=True)


# ==============================================================
# Copy data prep notebook
# ==============================================================
if False:
    # -------------------------------------------------
    # Query data from gcs
    # -------------------------------------------------
    in_path_gcs_base_file = 'gs://wx-personal/alexander/tripleM/tierMP.parquet'
    in_path_local_base_file = '/home/jovyan/a02_projects/a05_isd/data/data_t_mp.parquet'
    utl.Copy_Gsutil(in_path_gcs_base_file, in_path_local_base_file)

    # Query data
    df = pd.read_parquet(in_path_local_base_file)
    print(df.shape) # (884310, 24)
    df.head(2)
    df['ref_dt'] = df['ref_dt'].values.astype('datetime64')
    df['ref_dt'].value_counts() # 2019-05-05 00:00:00    884310
    df.to_parquet('/home/jovyan/a02_projects/a05_isd/data/base_t_mp.parquet', index=False)

    # Copy to working storage
    utl.Copy_Gsutil(
        '/home/jovyan/a02_projects/a05_isd/data/base_t_mp.parquet'
        ,'gs://wx-personal/joe/a02_decision-engine/t_mp/data/base_t_mp.parquet'
    )


# ==============================================================
# Paths
# ==============================================================
# Files
file_base = 'base_t_mp.parquet'
file_cmd_sources = 'cmd_sources.txt'

# GCS
path_gcs_prj = 'gs://wx-personal/joe/a02_decision-engine/t_mp'
path_gcs_config = f'{path_gcs_prj}/config'
path_gcs_data = f'{path_gcs_prj}/data'
path_gcs_data_cmd = f'{path_gcs_data}/w_cmd'

path_gcs_base_file = f'{path_gcs_data}/{file_base}'
path_gcs_cmd_sources_file = f'{path_gcs_config}/{file_cmd_sources}'

# Local
path_local_prj = '/home/jovyan/a02_projects/a05_isd'
path_local_data = f'{path_local_prj}/data'
path_local_base_file = f'{path_local_data}/{file_base}'
path_local_cmd_sources_file = f'{path_local_data}/{file_cmd_sources}'

#__model run argos
mdlTypeLs = ['open', 'rdm', 'spd', 'unsub']
path_local_run_open = f'{path_local_prj}/back_run_open.yaml'

# Sample base with cmd
file_base_smpl = 'base_t_mp_smpl.parquet'
path_local_base_smpl_file = f'{path_local_data}/{file_base_smpl}'
path_gcs_base_smpl_file = f'{path_gcs_data}/{file_base_smpl}'
path_gcs_data_cmd_smpl = f'{path_gcs_data}/w_cmd_smpl'


# -------------------------------------------------
# Load
# -------------------------------------------------
# utl.Copy_Gsutil(path_gcs_base_file, path_local_base_file)

base = pd.read_parquet(path_local_base_file)
print(base.shape) # (884310, 24)

baseSmpl = base.sample(utl.Get_N_Smpl(base))
baseSmpl.to_parquet(path_local_base_smpl_file, index=False)
utl.Copy_Gsutil(path_local_base_smpl_file, path_gcs_base_smpl_file)

# cmd_sources
utl.Copy_Gsutil(path_local_cmd_sources_file, path_gcs_cmd_sources_file)


# ==============================================================
# CMD2 pivoter
# ==============================================================
utl.Call_Table_Pivoter(
    in_path=path_gcs_base_file
    ,out_path=path_gcs_data_cmd
    ,file_incl_sources=path_gcs_cmd_sources_file
    ,out_file_num=1
)
# Sample base w cmd
utl.Call_Table_Pivoter(
    in_path=path_gcs_base_smpl_file
    ,out_path=path_gcs_data_cmd_smpl
    ,file_incl_sources=path_gcs_cmd_sources_file
    ,out_file_num=1
)
############ HERE ###############


# ==============================================================
# Create excel config
# ==============================================================
#> in ~/a01_repos/gen-akl-feature-config

# ==============================================================
# Model Build
# ==============================================================
for mt in mdlTypeLs:
    path_local_run = f'{path_local_prj}/t_mp_run_{mt}.yaml'
    subprocess.call(f'argo submit {path_local_run}', shell=True)

mt = 'unsub'
path_local_run = f'{path_local_prj}/t_mp_run_{mt}.yaml'
subprocess.call(f'argo submit {path_local_run}', shell=True)
    
!argo list | grep joe
!argo get joe-core-rn-9n25c
!kubectl logs joe-tss-open-6zhkc-1127591488 main #> 'errlog_akl_05_reg_2.txt'
# !argo delete joe-t-mp-open-78gk8

!argo delete joe-t-mp-unsub-4qppn
!argo delete joe-core-ro-4qhmw
!argo delete joe-core-ro-prev-dmj4d
!argo delete joe-t-mp-unsub-26zns
!argo delete joe-core-unsub-zjm4q
!argo delete joe-core-unsub-prevqwr4k
!argo delete joe-core-rn-9n25c
!argo delete joe-core-rn-prev-tv7vx


# ==============================================================
# Exploratory
# ==============================================================
base = pd.read_parquet(path_local_base_file)
print(base.shape) # (3111277, 37)
base.head(2)


# -------------------------------------------------
# Binary response proportions
# -------------------------------------------------
utl.Show_Count_Perc(base, 'open_flag')
utl.Show_Count_Perc(base, 'redeem_flag')
utl.Show_Count_Perc(base, 'unsub_flag')

# Spend (regression)
base['tot_amt_incld_gst_promo'].describe().round(2)
base.groupby('redeem_flag')['tot_amt_incld_gst_promo'].describe()


# -------------------------------------------------
# Unsubcribe model
# -------------------------------------------------
utl.Copy_Gsutil('gs://wx-personal/joe/a02_decision-engine/isd/data/w_cmd/part-00000-f293d9b4-6619-4bd1-8260-76d4b4759aad-c000.snappy.parquet', './data/w_cmd_part.parquet')

baseCmd = pd.read_parquet('./data/w_cmd_part.parquet')
print(baseCmd.shape)

colsUnsub = [
    'f22_unique_cvm_unsub_cnt_bigw'
    ,'f22_unique_cvm_unsub_cnt_bws'
    ,'f22_unique_cvm_unsub_cnt_caltex'
    ,'f22_unique_cvm_unsub_cnt_fuelco'
    ,'f22_unique_cvm_unsub_cnt_supers'
    
    # numeric
    ,'f22_last_open_send_hr_diff_b4_unsub'
]
for c in colsUnsub:
    print(baseCmd[c].value_counts())
